import java.util.Scanner;
//import hw4.*;


public class Test {
  public static void main(String[] args) {
    Scanner reader = new Scanner (System.in);
    int gameMode = reader.nextInt();
    
    if (gameMode==1) {
      new GUICF(new DylanAI());
    } else if (gameMode==2) {
      CFPlayer ai1 = new DylanAI();
      CFPlayer ai2 = new RandomAI();
      int n = 10000;
      int winCount = 0;
      for (int i =0; i < n ; i++) {
        ConsoleCF game = new ConsoleCF(ai1, ai2);
        game.playOut();
        if(game.getWinner() == ai1.getName())
          winCount++;
      }
      System.out.println(ai1.getName() + " wins " +((double) winCount)/n *100 + "% of the time.");
    } else {
      ConsoleCF game = new ConsoleCF(new DylanAI());
      game.playOut();
      System.out.println(game.getWinner() + " has won.");
    } 
  }
}