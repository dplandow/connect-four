//package hw4;

import java.util.Random;

public class RandomAI implements CFPlayer {
	public int nextMove(CFGame g) {
		Random x = new Random(System.currentTimeMillis());
		int random_column = x.nextInt(7) + 1;
		while (!g.is_playable(random_column)) {
			random_column = x.nextInt(7) + 1;
		}
		return random_column;
	}
	public String getName() {
		return "Random Player";
	}
}