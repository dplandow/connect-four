//package hw4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.CompoundBorder;

public class GUICF extends CFGame {
	public static void main(String[] args) {
		GUICF gui = new GUICF(new DylanAI());
	}
	private GameBoard this_board;
	private CFGame game;
	private CFPlayer red_player;
	private CFPlayer black_player;
	private JFrame frame;
	private java.awt.Container content_pane;
	public GUICF(CFPlayer ai) {
		frame = new JFrame("Connect Four");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		content_pane = frame.getContentPane();
		JButton arrow1 = new JButton("\u2193");
		arrow1.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		arrow1.addActionListener(new Arrow1_Clicked());
		JButton arrow2 = new JButton("\u2193");
		arrow2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		arrow2.addActionListener(new Arrow2_Clicked());
		JButton arrow3 = new JButton("\u2193");
		arrow3.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		arrow3.addActionListener(new Arrow3_Clicked());
		JButton arrow4 = new JButton("\u2193");
		arrow4.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		arrow4.addActionListener(new Arrow4_Clicked());
		JButton arrow5 = new JButton("\u2193");
		arrow5.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		arrow5.addActionListener(new Arrow5_Clicked());
		JButton arrow6 = new JButton("\u2193");
		arrow6.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		arrow6.addActionListener(new Arrow6_Clicked());
		JButton arrow7 = new JButton("\u2193");
		arrow7.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		arrow7.addActionListener(new Arrow7_Clicked());
		JPanel arrow_panel = new JPanel();
		content_pane.add(arrow_panel, BorderLayout.NORTH);
		arrow_panel.setLayout(new GridLayout(1,7));
		arrow_panel.add(arrow1);
		arrow_panel.add(arrow2);
		arrow_panel.add(arrow3);
		arrow_panel.add(arrow4);
		arrow_panel.add(arrow5);
		arrow_panel.add(arrow6);
		arrow_panel.add(arrow7);

		this_board = new GameBoard();
		game = new CFGame();

		Random x = new Random(System.currentTimeMillis());
		if (x.nextInt(2) == 0) {
			red_player = ai;
			black_player = null;
		}
		else  {
			red_player = null;
			black_player = ai;
		}
		if (red_player != null) {
			playGUI(red_player.nextMove(game));
		}

		frame.setSize(900, 600);
		frame.setVisible (true);

	}
	public GUICF(CFPlayer ai1, CFPlayer ai2) {
		JFrame frame = new JFrame("Connect Four");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		content_pane = frame.getContentPane();
		JButton play_button = new JButton("Play");
		play_button.addActionListener(new Play_Clicked());
		JPanel button_panel = new JPanel();
		content_pane.add(button_panel, BorderLayout.NORTH);
		button_panel.setLayout(new GridLayout(1, 1));
		button_panel.add(play_button);

		this_board = new GameBoard();
		game = new CFGame();

		Random x = new Random(System.currentTimeMillis());
		if (x.nextInt(2) == 0) {
			red_player = ai1;
			black_player = ai2;
		}
		else {
			red_player = ai2;
			black_player = ai1;
		}

		frame.setSize(900, 600);
		frame.setVisible (true);
	}
	private boolean playGUI(int c) {
		if (game.is_playable(c)) {
			int empty_slot = 0;
			for (int i = 0; i < 6; i++) {
				if (game.getState()[c - 1][i] == 0) {
					empty_slot = i;
					if (game.isRedTurn()) {
						this_board.paint(c - 1, i, 1);
						break;
					}
					else {
						this_board.paint(c - 1, i, -1);
						break;
					}
				}
			}
			game.play(c);
			return true;
		}
		else 
			return false;
	}
	private class GameBoard extends javax.swing.JPanel {
		private JLabel[][] labels = new JLabel[7][6];
		private GameBoard() {
			for (int i = 0; i < 7; i++) {
				for (int j = 0; j < 6; j++) {
					labels[i][j] = new JLabel();
					labels[i][j].setOpaque(true);
					Border black_border = BorderFactory.createLineBorder(Color.BLACK, 2);
					Border white_border = BorderFactory.createLineBorder(Color.WHITE, 1);
					CompoundBorder border = new CompoundBorder(white_border, black_border);
					labels[i][j].setBorder(border);
				}
			}

			JPanel square_panel = new JPanel();
			content_pane.add(square_panel, BorderLayout.CENTER);
			square_panel.setLayout(new GridLayout(6, 7));
			for (int i = 5; i >= 0; i--) {
				for (int j = 0; j < 7; j++) {
					square_panel.add(labels[j][i]);
				}
			}
		}
		private void paint(int x, int y, int color) {
			if (color == -1)
				labels[x][y].setBackground(Color.BLACK);
			else
				labels[x][y].setBackground(Color.RED);
		}
	}
	private class Play_Clicked implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (game.isGameOver()) {
				if (game.winner() == 1)
					System.out.println("Winner: " + red_player.getName());
				else if (game.winner() == -1)
					System.out.println("Winner: " + black_player.getName());
				else
					System.out.println("Draw");
				System.exit(0);
			}
			if (!game.isGameOver()) {
				if (game.isRedTurn()) {
					playGUI(red_player.nextMove(game)); 
				}
				else {
					playGUI(black_player.nextMove(game));
				}
			}
		}
	}
	private class Arrow1_Clicked implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean is_red = game.isRedTurn();
			boolean is_black = !game.isRedTurn();
			if (game.isGameOver()) {
				if (game.winner() == 1 && red_player != null)
					System.out.println("Winner: " + red_player.getName());
				else if (game.winner() == -1 && black_player != null)
					System.out.println("Winner: " + black_player.getName());
				else if (game.winner() == 0)
					System.out.println("Draw");
				else
					System.out.println("Winner: Human");

				System.exit(0);
			}
			if (!game.isGameOver() && game.is_playable(1) && ((game.isRedTurn() && red_player == null) || (!game.isRedTurn() && black_player == null))) {
				playGUI(1);
			}
			if (((is_red && !game.isRedTurn()) || (is_black && game.isRedTurn())) && !game.isGameOver()) {
				if (game.isRedTurn() && red_player != null)
					playGUI(red_player.nextMove(game));
				else if (black_player != null)
					playGUI(black_player.nextMove(game));
			}  
		}
	}
	private class Arrow2_Clicked implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean is_red = game.isRedTurn();
			boolean is_black = !game.isRedTurn();
			if (game.isGameOver()) {
				if (game.winner() == 1 && red_player != null)
					System.out.println("Winner: " + red_player.getName());
				else if (game.winner() == -1 && black_player != null)
					System.out.println("Winner: " + black_player.getName());
				else if (game.winner() == 0)
					System.out.println("Draw");
				else
					System.out.println("Winner: Human");

				System.exit(0);
			}
			if (!game.isGameOver() && game.is_playable(2) && ((game.isRedTurn() && red_player == null) || (!game.isRedTurn() && black_player == null))) {
				playGUI(2);
			}
			if (((is_red && !game.isRedTurn()) || (is_black && game.isRedTurn())) && !game.isGameOver()) {
				if (game.isRedTurn() && red_player != null)
					playGUI(red_player.nextMove(game));
				else if (black_player != null)
					playGUI(black_player.nextMove(game));
			} 
		}
	}
	private class Arrow3_Clicked implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean is_red = game.isRedTurn();
			boolean is_black = !game.isRedTurn();
			if (game.isGameOver()) {
				if (game.winner() == 1 && red_player != null)
					System.out.println("Winner: " + red_player.getName());
				else if (game.winner() == -1 && black_player != null)
					System.out.println("Winner: " + black_player.getName());
				else if (game.winner() == 0)
					System.out.println("Draw");
				else
					System.out.println("Winner: Human");

				System.exit(0);
			}
			if (!game.isGameOver() && game.is_playable(3) && ((game.isRedTurn() && red_player == null) || (!game.isRedTurn() && black_player == null))) {
				playGUI(3);
			}
			if (((is_red && !game.isRedTurn()) || (is_black && game.isRedTurn())) && !game.isGameOver()) {
				if (game.isRedTurn() && red_player != null)
					playGUI(red_player.nextMove(game));
				else if (black_player != null)
					playGUI(black_player.nextMove(game));
			} 
		}
	}
	private class Arrow4_Clicked implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean is_red = game.isRedTurn();
			boolean is_black = !game.isRedTurn();
			if (game.isGameOver()) {
				if (game.winner() == 1 && red_player != null)
					System.out.println("Winner: " + red_player.getName());
				else if (game.winner() == -1 && black_player != null)
					System.out.println("Winner: " + black_player.getName());
				else if (game.winner() == 0)
					System.out.println("Draw");
				else
					System.out.println("Winner: Human");

				System.exit(0);
			}
			if (!game.isGameOver() && game.is_playable(4) && ((game.isRedTurn() && red_player == null) || (!game.isRedTurn() && black_player == null))) {
				playGUI(4);
			}
			if (((is_red && !game.isRedTurn()) || (is_black && game.isRedTurn())) && !game.isGameOver()) {
				if (game.isRedTurn() && red_player != null)
					playGUI(red_player.nextMove(game));
				else if (black_player != null)
					playGUI(black_player.nextMove(game));
			} 
		}
	}
	private class Arrow5_Clicked implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean is_red = game.isRedTurn();
			boolean is_black = !game.isRedTurn();
			if (game.isGameOver()) {
				if (game.winner() == 1 && red_player != null)
					System.out.println("Winner: " + red_player.getName());
				else if (game.winner() == -1 && black_player != null)
					System.out.println("Winner: " + black_player.getName());
				else if (game.winner() == 0)
					System.out.println("Draw");
				else
					System.out.println("Winner: Human");

				System.exit(0);
			}
			if (!game.isGameOver() && game.is_playable(5) && ((game.isRedTurn() && red_player == null) || (!game.isRedTurn() && black_player == null))) {
				playGUI(5);
			}
			if (((is_red && !game.isRedTurn()) || (is_black && game.isRedTurn())) && !game.isGameOver()) {
				if (game.isRedTurn() && red_player != null)
					playGUI(red_player.nextMove(game));
				else if (black_player != null)
					playGUI(black_player.nextMove(game));
			} 
		}
	}
	private class Arrow6_Clicked implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean is_red = game.isRedTurn();
			boolean is_black = !game.isRedTurn();
			if (game.isGameOver()) {
				if (game.winner() == 1 && red_player != null)
					System.out.println("Winner: " + red_player.getName());
				else if (game.winner() == -1 && black_player != null)
					System.out.println("Winner: " + black_player.getName());
				else if (game.winner() == 0)
					System.out.println("Draw");
				else
					System.out.println("Winner: Human");

				System.exit(0);
			}
			if (!game.isGameOver() && game.is_playable(6) && ((game.isRedTurn() && red_player == null) || (!game.isRedTurn() && black_player == null))) {
				playGUI(6);
			}
			if (((is_red && !game.isRedTurn()) || (is_black && game.isRedTurn())) && !game.isGameOver()) {
				if (game.isRedTurn() && red_player != null)
					playGUI(red_player.nextMove(game));
				else if (black_player != null)
					playGUI(black_player.nextMove(game));
			} 
		}
	}
	private class Arrow7_Clicked implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean is_red = game.isRedTurn();
			boolean is_black = !game.isRedTurn();
			if (game.isGameOver()) {
				if (game.winner() == 1 && red_player != null)
					System.out.println("Winner: " + red_player.getName());
				else if (game.winner() == -1 && black_player != null)
					System.out.println("Winner: " + black_player.getName());
				else if (game.winner() == 0)
					System.out.println("Draw");
				else
					System.out.println("Winner: Human");

				System.exit(0);
			}
			if (!game.isGameOver() && game.is_playable(7) && ((game.isRedTurn() && red_player == null) || (!game.isRedTurn() && black_player == null))) {
				playGUI(7);
			}
			if (((is_red && !game.isRedTurn()) || (is_black && game.isRedTurn())) && !game.isGameOver()) {
				if (game.isRedTurn() && red_player != null)
					playGUI(red_player.nextMove(game));
				else if (black_player != null)
					playGUI(black_player.nextMove(game));
			} 
		}
	}
}