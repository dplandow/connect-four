//package hw4;

public class DylanAI implements CFPlayer{
	public int nextMove(CFGame g) {
		int in_a_row = 0;
		int check_color = 0;
		int block_color = 0;
		int[][] state = g.getState();
		if (g.isRedTurn()) {
			check_color = 1;
			block_color = -1;
		}
		else {
			check_color = -1;
			block_color = 1;
		}
		for (int i = 0; i < 6; i++) { // check for horizontal connect 4
			for (int j = 0; j < 7; j++) {
		      	if (state[j][i] == check_color) {
		        	in_a_row++;
		      	}
		      	else
		        	in_a_row = 0;
	      		if (in_a_row >= 2 && (j - 3) >= 0 && state[j - 3][i] == check_color && state[j - 2][i] == 0) {
	      			if (is_row_full(g, j - 2, i))
	      				return (j - 1);
	      		}
	      		if (in_a_row >= 2 && (j + 2) < 7 && state[j + 2][i] == check_color && state[j + 1][i] == 0) {
	      			if (is_row_full(g, j + 1, i))
	      				return (j + 2);
	      		}
		      	if (in_a_row >= 3 && (j + 1) < 7 && state[j + 1][i] == 0) {
	      			if (is_row_full(g, j + 1, i))
	      				return (j + 2);
		      	}
		      	if (in_a_row >= 3 && (j - 3) >= 0 && state[j - 3][i] == 0) {
	      			if (is_row_full(g, j - 3, i))
	      				return (j - 2);
		      	}
		    }
	    }

	    in_a_row = 0;

	    for (int i = 0; i < 7; i++)  { // check for vertical connect 4
	    	for (int j = 0; j < 6; j++) {
		      	if (state[i][j] == check_color)
		        	in_a_row++;
		      	else
		        	in_a_row = 0;
		      	if (in_a_row >= 3 && (j + 1) < 6 && state[i][j + 1] == 0) {
		        	return (i + 1);
		      	}
		    }
	    }

	    in_a_row = 0;

	    for (int i = 0; i < 4; i++) {
	    	in_a_row = 0;
	    	int row, col;
	    	for (col = i, row = 0; col < 7 && row < 6; col++, row++) {
	    		if (state[col][row] == check_color)
	    			in_a_row++;
	    		else
	    			in_a_row = 0;
	    		if (in_a_row >= 2 && (col - 3) >= 0 && (row - 3) >= 0 && state[col - 3][row - 3] == check_color && state[col - 2][row - 2] == 0) {
	    			if (is_row_full(g, col - 2, row - 2))
	    				return (col - 1);
	    		}
	    		if (in_a_row >= 2 && (col + 2) < 7 && (row + 2) < 6 && state[col + 2][row + 2] == check_color && state[col + 1][row + 1] == 0) {
	    			if (is_row_full(g, col + 1, row + 1))
	    				return (col + 2);
	    		}
	    		if (in_a_row >= 3 && (col + 1) < 7 && (row + 1) < 6 && state[col + 1][row + 1] == 0) {
	    			if (is_row_full(g, col + 1, row + 1))
	    				return (col + 2);
	    		}
	    		if (in_a_row >= 3 && (col - 3) >= 0 && (row - 3) >= 0 && state[col - 3][row - 3] == 0) {
	    			if (is_row_full(g, col - 3, row - 3))
	    				return (col - 2);
	    		}
	    	}
	    }
	    for (int i = 1; i <= 2; i++) {
	    	in_a_row = 0;
	    	int row, col;
	    	for (col = 0, row = i; col < 7 && row < 6; col++, row++) {
	    		if (state[col][row] == check_color)
	    			in_a_row++;
	    		else
	    			in_a_row = 0;
	    		if (in_a_row >= 2 && (col - 3) >= 0 && (row - 3) >= 0 && state[col - 3][row - 3] == check_color && state[col - 2][row - 2] == 0) {
	    			if (is_row_full(g, col - 2, row - 2))
	    				return (col - 1);
	    		}
	    		if (in_a_row >= 2 && (col + 2) < 7 && (row + 2) < 6 && state[col + 2][row + 2] == check_color && state[col + 1][row + 1] == 0) {
	    			if (is_row_full(g, col + 1, row + 1))
	    				return (col + 2);
	    		}
	    		if (in_a_row >= 3 && (col + 1) < 7 && (row + 1) < 6 && state[col + 1][row + 1] == 0) {
	    			if (is_row_full(g, col + 1, row + 1))
	    				return (col + 2);
	    		}
	    		if (in_a_row >= 3 && (col - 3) >= 0 && (row - 3) >= 0 && state[col - 3][row - 3] == 0) {
	    			if (is_row_full(g, col - 3, row - 3))
	    				return (col - 2);
	    		}
	    	}
	    }
	    for (int i = 6; i >= 3; i--){
			in_a_row = 0;
			int row, col;
			for (col = i, row = 0; col >= 0 && row < 6; col--, row++) {
				if (state[col][row] == check_color)
	    			in_a_row++;
	    		else
	    			in_a_row = 0;
	    		if (in_a_row >= 2 && (col + 3) < 7 && (row - 3) >= 0 && state[col + 3][row - 3] == check_color && state[col + 2][row - 2] == 0) {
	    			if (is_row_full(g, col + 2, row - 2))
	    				return (col + 3);
	    		}
	    		if (in_a_row >= 2 && (col - 2) >= 0 && (row + 2) < 6 && state[col - 2][row + 2] == check_color && state[col - 1][row + 1] == 0) {
	    			if (is_row_full(g, col - 1, row + 1))
	    				return (col);
	    		}
	    		if (in_a_row >= 3 && (col - 1) >= 0 && (row + 1) < 6 && state[col - 1][row + 1] == 0) {
	    			if (is_row_full(g, col - 1, row + 1))
	    				return (col);
	    		}
	    		if (in_a_row >= 3 && (col + 3) < 7 && (row - 3) >= 0 && state[col + 3][row - 3] == 0) {
	    			if (is_row_full(g, col + 3, row - 3))
	    				return (col + 4);
	    		}
			}
	    }
	    for (int i = 1; i <= 2; i++){
			in_a_row = 0;
			int row, col;
			for (col = 6, row = i; col >= 0 && row < 6; col--, row++) {
				if (state[col][row] == check_color)
	    			in_a_row++;
	    		else
	    			in_a_row = 0;
	    		if (in_a_row >= 2 && (col + 3) < 7 && (row - 3) >= 0 && state[col + 3][row - 3] == check_color && state[col + 2][row - 2] == 0) {
	    			if (is_row_full(g, col + 2, row - 2))
	    				return (col + 3);
	    		}
	    		if (in_a_row >= 2 && (col - 2) >= 0 && (row + 2) < 6 && state[col - 2][row + 2] == check_color && state[col - 1][row + 1] == 0) {
	    			if (is_row_full(g, col - 1, row + 1))
	    				return (col);
	    		}
	    		if (in_a_row >= 3 && (col - 1) >= 0 && (row + 1) < 6 && state[col - 1][row + 1] == 0) {
	    			if (is_row_full(g, col - 1, row + 1))
	    				return (col);
	    		}
	    		if (in_a_row >= 3 && (col + 3) < 7 && (row - 3) >= 0 && state[col + 3][row - 3] == 0) {
	    			if (is_row_full(g, col + 3, row - 3))
	    				return (col + 4);
	    		}
			}
	    }


	    for (int i = 0; i < 6; i++) { // check for horizontal connect 4 to block
			for (int j = 0; j < 7; j++) {
		      	if (state[j][i] == block_color) {
		        	in_a_row++;
		      	}
		      	else
		        	in_a_row = 0;
	      		if (in_a_row >= 2 && (j - 3) >= 0 && state[j - 3][i] == block_color && state[j - 2][i] == 0) {
	      			if (is_row_full(g, j - 2, i))
	      				return (j - 1);
	      		}
	      		if (in_a_row >= 2 && (j + 2) < 7 && state[j + 2][i] == block_color && state[j + 1][i] == 0) {
	      			if (is_row_full(g, j + 1, i))
	      				return (j + 2);
	      		}
		      	if (in_a_row >= 3 && (j + 1) < 7 && state[j + 1][i] == 0) {
	      			if (is_row_full(g, j + 1, i))
	      				return (j + 2);
		      	}
		      	if (in_a_row >= 3 && (j - 3) >= 0 && state[j - 3][i] == 0) {
	      			if (is_row_full(g, j - 3, i))
	      				return (j - 2);
		      	}
		    }
	    }

	    in_a_row = 0;

	    for (int i = 0; i < 7; i++)  { // check for vertical connect 4 to block
	    	for (int j = 0; j < 6; j++) {
		      	if (state[i][j] == block_color)
		        	in_a_row++;
		      	else
		        	in_a_row = 0;
		      	if (in_a_row >= 3 && (j + 1) < 6 && state[i][j + 1] == 0) {
		        	return (i + 1);
		      	}
		    }
	    }

	    in_a_row = 0;

	    for (int i = 0; i < 4; i++) {
	    	in_a_row = 0;
	    	int row, col;
	    	for (col = i, row = 0; col < 7 && row < 6; col++, row++) {
	    		if (state[col][row] == block_color)
	    			in_a_row++;
	    		else
	    			in_a_row = 0;
	    		if (in_a_row >= 2 && (col - 3) >= 0 && (row - 3) >= 0 && state[col - 3][row - 3] == block_color && state[col - 2][row - 2] == 0) {
	    			if (is_row_full(g, col - 2, row - 2))
	    				return (col - 1);
	    		}
	    		if (in_a_row >= 2 && (col + 2) < 7 && (row + 2) < 6 && state[col + 2][row + 2] == block_color && state[col + 1][row + 1] == 0) {
	    			if (is_row_full(g, col + 1, row + 1))
	    				return (col + 2);
	    		}
	    		if (in_a_row >= 3 && (col + 1) < 7 && (row + 1) < 6 && state[col + 1][row + 1] == 0) {
	    			if (is_row_full(g, col + 1, row + 1))
	    				return (col + 2);
	    		}
	    		if (in_a_row >= 3 && (col - 3) >= 0 && (row - 3) >= 0 && state[col - 3][row - 3] == 0) {
	    			if (is_row_full(g, col - 3, row - 3))
	    				return (col - 2);
	    		}
	    	}
	    }
	    for (int i = 1; i <= 2; i++) {
	    	in_a_row = 0;
	    	int row, col;
	    	for (col = 0, row = i; col < 7 && row < 6; col++, row++) {
	    		if (state[col][row] == block_color)
	    			in_a_row++;
	    		else
	    			in_a_row = 0;
	    		if (in_a_row >= 2 && (col - 3) >= 0 && (row - 3) >= 0 && state[col - 3][row - 3] == block_color && state[col - 2][row - 2] == 0) {
	    			if (is_row_full(g, col - 2, row - 2))
	    				return (col - 1);
	    		}
	    		if (in_a_row >= 2 && (col + 2) < 7 && (row + 2) < 6 && state[col + 2][row + 2] == block_color && state[col + 1][row + 1] == 0) {
	    			if (is_row_full(g, col + 1, row + 1))
	    				return (col + 2);
	    		}
	    		if (in_a_row >= 3 && (col + 1) < 7 && (row + 1) < 6 && state[col + 1][row + 1] == 0) {
	    			if (is_row_full(g, col + 1, row + 1))
	    				return (col + 2);
	    		}
	    		if (in_a_row >= 3 && (col - 3) >= 0 && (row - 3) >= 0 && state[col - 3][row - 3] == 0) {
	    			if (is_row_full(g, col - 3, row - 3))
	    				return (col - 2);
	    		}
	    	}
	    }
	    for (int i = 6; i >= 3; i--){
			in_a_row = 0;
			int row, col;
			for (col = i, row = 0; col >= 0 && row < 6; col--, row++) {
				if (state[col][row] == block_color)
	    			in_a_row++;
	    		else
	    			in_a_row = 0;
	    		if (in_a_row >= 2 && (col + 3) < 7 && (row - 3) >= 0 && state[col + 3][row - 3] == block_color && state[col + 2][row - 2] == 0) {
	    			if (is_row_full(g, col + 2, row - 2))
	    				return (col + 3);
	    		}
	    		if (in_a_row >= 2 && (col - 2) >= 0 && (row + 2) < 6 && state[col - 2][row + 2] == block_color && state[col - 1][row + 1] == 0) {
	    			if (is_row_full(g, col - 1, row + 1))
	    				return (col);
	    		}
	    		if (in_a_row >= 3 && (col - 1) >= 0 && (row + 1) < 6 && state[col - 1][row + 1] == 0) {
	    			if (is_row_full(g, col - 1, row + 1))
	    				return (col);
	    		}
	    		if (in_a_row >= 3 && (col + 3) < 7 && (row - 3) >= 0 && state[col + 3][row - 3] == 0) {
	    			if (is_row_full(g, col + 3, row - 3))
	    				return (col + 4);
	    		}
			}
	    }
	    for (int i = 1; i <= 2; i++){
			in_a_row = 0;
			int row, col;
			for (col = 6, row = i; col >= 0 && row < 6; col--, row++) {
				if (state[col][row] == block_color)
	    			in_a_row++;
	    		else
	    			in_a_row = 0;
	    		if (in_a_row >= 2 && (col + 3) < 7 && (row - 3) >= 0 && state[col + 3][row - 3] == block_color && state[col + 2][row - 2] == 0) {
	    			if (is_row_full(g, col + 2, row - 2))
	    				return (col + 3);
	    		}
	    		if (in_a_row >= 2 && (col - 2) >= 0 && (row + 2) < 6 && state[col - 2][row + 2] == block_color && state[col - 1][row + 1] == 0) {
	    			if (is_row_full(g, col - 1, row + 1))
	    				return (col);
	    		}
	    		if (in_a_row >= 3 && (col - 1) >= 0 && (row + 1) < 6 && state[col - 1][row + 1] == 0) {
	    			if (is_row_full(g, col - 1, row + 1))
	    				return (col);
	    		}
	    		if (in_a_row >= 3 && (col + 3) < 7 && (row - 3) >= 0 && state[col + 3][row - 3] == 0) {
	    			if (is_row_full(g, col + 3, row - 3))
	    				return (col + 4);
	    		}
			}
	    }

	    RandomAI random_player = new RandomAI();
	    return random_player.nextMove(g);
	}

	public String getName() {
		return "Dylan's AI";
	}

	private boolean is_row_full(CFGame g, int column, int row) {
		boolean is_full_below = true;
  		for (int k = 0; k < row; k++) {
  			if(g.getState()[column][k] == 0) {
  				is_full_below = false;
  				break;
  			}
  		}
  		return is_full_below;
	}
}