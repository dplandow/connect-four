//package hw4;

import java.util.Random;
import java.util.Scanner;

public class ConsoleCF extends CFGame {
	private CFGame game;
	private CFPlayer red_player;
	private CFPlayer black_player;

	public ConsoleCF(CFPlayer ai) {
		game = new CFGame();
		Random x = new Random(System.currentTimeMillis());
		if (x.nextInt(2) == 0) {
			red_player = ai;
			black_player = new HumanPlayer();
		}
		else  {
			red_player = new HumanPlayer();
			black_player = ai;
		}
	}
	public ConsoleCF(CFPlayer ai1, CFPlayer ai2) {
		game = new CFGame();
		Random x = new Random(System.currentTimeMillis());
		if (x.nextInt(2) == 0) {
			red_player = ai1;
			black_player = ai2;
		}
		else  {
			red_player = ai2;
			black_player = ai1;
		}
	}

	public void playOut() {
		while (!game.isGameOver()) {
			game.play(red_player.nextMove(game));
			if (!game.isGameOver())
				game.play(black_player.nextMove(game));
		}
		game.print();
		System.out.println("Winner: " + this.getWinner());
	}

	public String getWinner() {
		if (game.winner() == 1) 
			return red_player.getName();
		else if (game.winner() == -1)
			return black_player.getName();
		else
			return "draw";
	}

	private class HumanPlayer implements CFPlayer {
		public int nextMove(CFGame g) {
			g.print();
			int input;
			Scanner in = new Scanner(System.in);
			System.out.print("Enter next move: ");
			input = in.nextInt();
			while (!game.is_playable(input)) {
				System.out.print("Invalid move. Enter next move: ");
				input = in.nextInt();
			}
			return input;
		}
		public String getName() {
			return "Human Player";
		}
	}

}