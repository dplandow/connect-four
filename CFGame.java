//package hw4;

public class CFGame {
  //state[i][j]= 0 means the i,j slot is empty
  //state[i][j]= 1 means the i,j slot has red
  //state[i][j]=-1 means the i,j slot has black
  private final int[][] state;
  private boolean isRedTurn;
  private int last_col = 0;
  private int last_row = 0;
  private int winner = 0;
  
  {
    state = new int[7][6];
    for (int i=0; i<7; i++)
      for (int j=0; j<6; j++)
        state[i][j] = 0;
    isRedTurn = true; //red goes first
  }
    
  public int[][] getState() {
    int[][] ret_arr = new int[7][6];
    for (int i=0; i<7; i++)
      for (int j=0; j<6; j++)
        ret_arr[i][j] = state[i][j];
    return ret_arr;
  }
  
  public boolean isRedTurn() {
    return isRedTurn;
  }

  public boolean is_playable(int column) {
    if (column < 1 || column > 7)
      return false;
    for (int i = 0; i < 6; i++) {
      if (state[column - 1][i] == 0)
        return true;
    }
    return false;
  }
  
  public boolean play(int column) {
    if (!this.is_playable(column))
      return false;
    int i;
    for (i = 0; i < 6; i++) {
      if (state[column - 1][i] == 0)
        break;
    }
    if (this.isRedTurn() == true) {
      state[column - 1][i] = 1;
      last_row = i;
      last_col = column - 1;
      isRedTurn = false;
    }
    else {
      state[column - 1 ][i] = -1;
      last_row = i;
      last_col = column - 1;
      isRedTurn = true;
    }
    return true;
  }
  
  public boolean isGameOver() {
    int in_a_row = 0;
    int last_play_color;
    if (this.isRedTurn() == true)
      last_play_color = -1;
    else
      last_play_color = 1;
    for (int i = 0; i < 7; i++) { // check for horizontal connect 4
      if (state[i][last_row] == last_play_color) {
        in_a_row++;
        if (in_a_row >= 4) {
          winner = last_play_color;
          return true;
        }
      }
      else
        in_a_row = 0;
    }
    in_a_row = 0;
    for (int i = 0; i < 6; i++)  { // check for vertical connect 4
      if (state[last_col][i] == last_play_color) {
        in_a_row++;
        if (in_a_row >= 4) {
          winner = last_play_color;
          return true;
        }
      }
      else
        in_a_row = 0;
    }
    in_a_row = 0;
    
    for (int i = 0; i < 4; i++) {
      in_a_row = 0;
      int row, col;
      for (col = i, row = 0; col < 7 && row < 6; col++, row++) {
        if (state[col][row] == last_play_color) {
          in_a_row++;
          if (in_a_row >= 4) {
            winner = last_play_color;
            return true;
          }
        }
        else
          in_a_row = 0;
      }
    }
    for (int i = 1; i <= 2; i++) {
      in_a_row = 0;
      int row, col;
      for (col = 0, row = i; col < 7 && row < 6; col++, row++) {
        if (state[col][row] == last_play_color) {
          in_a_row++;
          if (in_a_row >= 4) {
            winner = last_play_color;
            return true;
          }
        }
        else
          in_a_row = 0;
      }
    }
    for (int i = 6; i >= 3; i--){
      in_a_row = 0;
      int row, col;
      for (col = i, row = 0; col >= 0 && row < 6; col--, row++) {
        if (state[col][row] == last_play_color) {
          in_a_row++;
          if (in_a_row >= 4) {
            winner = last_play_color;
            return true;
          }
        }
        else
          in_a_row = 0;
      }
    }
    for (int i = 1; i <= 2; i++){
      in_a_row = 0;
      int row, col;
      for (col = 6, row = i; col >= 0 && row < 6; col--, row++) {
        if (state[col][row] == last_play_color) {
          in_a_row++;
          if (in_a_row >= 4) {
            winner = last_play_color;
            return true;
          }
        }
        else
          in_a_row = 0;
      }
    }

    boolean full_board = true;
    for (int i = 0; i < 7; i++) {
      for (int j = 0; j < 6; j++) {
        if (state[i][j] == 0)
          full_board = false;
      }
    }

    if (full_board)
      return true;
    else
      return false;
  }
  
  public int winner() {
    if (winner == -1 || winner == 1)
      return winner;
    else 
      return 0;
  }
  public void print() {
    for (int i = 5; i >= 0; i--) {
      for (int j = 0; j < 7; j++) {
        if (state[j][i] == 1)
          System.out.print("[R] ");
        else if (state[j][i] == -1)
          System.out.print("[B] ");
        else
          System.out.print("[ ] ");
      }
      System.out.print("\n");
    }
  }
}
